from flask import Flask, jsonify, request
from app.models.series_model import SeriesModel


def series_route(app: Flask):


    @app.route("/series", methods=["POST"])
    def create():
        data = request.get_json()
        serie = SeriesModel(**data)
        return jsonify(serie.create_series()), 201

    
    @app.route("/series")
    def series():
      return jsonify(SeriesModel.get_series_all()), 200


    @app.route("/series/<int:id>")
    def select_by_id(id):
        return jsonify(SeriesModel.get_series_id(id)), 200