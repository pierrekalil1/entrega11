import psycopg2
from dotenv import load_dotenv
from os import getenv

load_dotenv()
configs = {
  "host": getenv("HOST"),
  "database": getenv("BD"),
  "user": getenv("USER"),
  "password": getenv("PASS")
}

