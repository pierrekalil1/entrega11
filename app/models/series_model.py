import psycopg2 
from dotenv import load_dotenv
from os import getenv


load_dotenv()

configs = {
  "host": getenv("HOST"),
  "database": getenv("BD"),
  "user": getenv("USER"),
  "password": getenv("PASSWORD")
}


class SeriesModel():

    series_keys = ["id", "serie", "seasons", "release_date", "genre", "imdb_rating"]
    def __init__(self, serie, seasons, release_date, genre, imdb_rating)  -> None:
        

        self.serie = serie.title()
        self.seasons = seasons 
        self.release_date = release_date 
        self.genre = genre.title()
        self.imdb_rating = imdb_rating 

    
    @staticmethod
    def get_series_all():
        # conn = psycopg2.connect(**configs)
        conn = psycopg2.connect(host="localhost", database="kenzie_serie",
                            user="pierre", password="1234")
        try:
            cur = conn.cursor()

            cur.execute(
              """
              SELECT * FROM ka_series;
              """
            )
            series = cur.fetchall()
            print(series)

            conn.commit()
            cur.close()
            conn.close()

            series_info = [dict(zip(SeriesModel.series_keys, serie)) for serie in series]

            return {"data": series_info}

        except psycopg2.DatabaseError:
            return {"data": []}
    

    @staticmethod
    def get_series_id(id):
        # conn = psycopg2.connect(**configs)
        conn = psycopg2.connect(host="localhost", database="kenzie_serie",
                            user="pierre", password="1234")
        
        try:
            cur = conn.cursor()

            cur.execute(
            """
            SELECT * FROM ka_series WHERE id=(%s);
            """, (id,)
            )
            serie = cur.fetchone()
            serie_info = [dict(zip(SeriesModel.series_keys, serie))]

            conn.commit()
            cur.close()
            conn.close()

            return {"data": serie_info }
        except psycopg2.DatabaseError:
            return {"error": "Not Found"}
        
    
    def create_series(self):
        print(self)
        # conn = psycopg2.connect(**configs)

        conn = psycopg2.connect(host="localhost", database="kenzie_serie",
                            user="pierre", password="1234")
        try:
            cur = conn.cursor()
            cur.execute("""
                CREATE TABLE IF NOT EXISTS ka_series (
                    id BIGSERIAL PRIMARY KEY,
                    serie VARCHAR(100) NOT NULL UNIQUE,
                    seasons INTEGER NOT NULL,
                    release_date DATE NOT NULL,
                    genre VARCHAR(50) NOT NULL,
                    imdb_rating FLOAT NOT NULL
            );
            """)
            query = 'INSERT INTO ka_series (serie, seasons, release_date, genre, imdb_rating) VALUES (%s, %s, %s, %s, %s)'
            query_values = list(self.__dict__.values())

            cur.execute(query, query_values)
            conn.commit()
            cur.close()
            conn.close()

            serie_info = dict(zip(SeriesModel.series_keys, query_values))

            return serie_info

        except psycopg2.DatabaseError:
            return {"error": "Serie já foi cadastrada"}

    
    

    
