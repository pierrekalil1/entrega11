from flask import Flask



def init_app(app: Flask):
    from app.models.series_model import series_model
    series_model(app)

    return app